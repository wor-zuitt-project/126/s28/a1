const Course = require("../models/course");


//Create a new course
module.exports.addCourse = (body) => {
	let newCourse = new Course ({
		name: body.name,
		description: body.description,
		price: body.price
		// createOn: body.createOn
		// 10 refers to a concept called "salt" which is the number of 
		// times encryption is run on the password
	})

	return newCourse.save().then((course, error) => {
		if(error){
			return false // course was not save
		} else {
			return true //Successfully save
		}
	})
}


//get All course
module.exports.getCourses = () => {
	return Course.find({}).then(result => {
		return result; //Find all courses, then return result
	})
}


//get specfic course
module.exports.getCourse = (params) => {
	//findById is a Mongoose operation that just finds a document by its ID
	return Course.findById(params.courseId).then(result => {
		return result;
	})
}


//update specific course
//Why 2 things. Because 1) What course we want to update? -> Param
// 2) How we update? -> body
module.exports.updateCourse = (params, body) => {
	let updatedCourse = {
		name: body.name,
		description: body.description,
		price: body.price
	}

	return Course.findByIdAndUpdate(params.courseId, updatedCourse).then((
		course, err) => {

		//error handling
		if(err){
			return false;
		} else {
			return true;
		}
	})
}




//archive Course (Active to Inactive)

module.exports.archiveCourse = (params, body) => {

	let archiveCourse = {
		isActive: false
	}

	return Course.findByIdAndUpdate(params.courseId, archiveCourse).then((
		course, err) => {

		//error handling
		if(err){
			return false;
		} else {
			return true;
		}
	})


}
