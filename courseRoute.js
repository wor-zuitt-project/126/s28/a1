const express = require('express')
const router = express.Router();
const courseController = require("../controllers/course")
const auth = require("../auth")

//route to create a new course 
// auth.verify will block all the code if the code hasn't pass the verification yet
router.post("/", auth.verify, (req, res) => {
		

		//Now the token is good, so we can decode it and check whether it's admin
		// auth.decode turns out token into a decoded Javascript object 

		if (auth.decode(req.headers.authorization).isAdmin) {
			courseController.addCourse(req.body).then(resultFromController => res.send(
			resultFromController))
		} else {
			res.send("Fail")


		}
		



		//If a non-admin tries, it simply receive an error message saying {auth: fail}
		// console.log(auth.decode(req.headers.authorization))


		//We can also request with Token
		//There are things that only admin can do, so we need token.
		// It contains isAdmin information

		// The last 3 of the Token is Authorization header and we use that to check
		// console.log(req.headers.authorization)
		// console.log(req.body)

})




	//Activity instructions: 
	// Create a route + controller function to create a new course,
	//complete with proper response forom the server once the opeation resolves

	//Make sure to use Postman to send a request to test/create your course



	// courseController.addCourse()

//route for getting all course 

router.get("/", (req, res) => {
	console.log(req)
	courseController.getCourses().then(resultFromController => res.send(
		resultFromController))
})


// //route for getting a single course 
// router.get("/:courseId", (req, res) => {
// 	console.log(req.body)
// 	console.log(req.params)
// 	//gonna get {endpoint: request}
// 	 {
// 	 When we use it http://localhost:4000/courses/618bd466fbae3ebf8bfe3409 
// 	 The lastest is ID
// 	 So we get {"food": "618bd466fbae3ebf8bfe3409"}
// 	}
	
// 	res.send(req.params)
// })


//route for getting a single course
router.get("/:courseId", (req, res)=> {
	courseController.getCourse(req.params).then(resultFromController => res.send 
				(resultFromController))
})


//update existing course
router.put("/:courseId", auth.verify, (req, res) => {
	if (auth.decode(req.headers.authorization).isAdmin) {
			courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController)
				)
		} else {
			res.send({auth: "failed"})


		}	
})


router.delete("/:courseId", (req, res) => {
	/*	
		ACTIVITY:
		Create a route for archiving a specific course with the 
		following specifications:

		1) The course to archive is determined by the ID passed in the URL

		2) Router must have token verification middleware

		3) Only admins must be alloed to archive courses

		4). Courses are NOT actually deleted, only their isActive fields change from true to false
	
		5) Try on Postman
	*/

	
	if (auth.decode(req.headers.authorization).isAdmin) {
			courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController)
				)
		} else {
			res.send({auth: "failed"})


		}	
	


})

module.exports = router;